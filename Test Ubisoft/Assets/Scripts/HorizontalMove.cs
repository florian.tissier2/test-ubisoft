﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HorizontalMove : MonoBehaviour {
    [SerializeField] Vector2 clampingPositions;
    [SerializeField] float speed = 1.5f;
    [SerializeField] float gyroSpeed = 1.5f;

    Camera mainCamera;
    Vector3 previousPos = Vector3.zero;
    float initialGyroPosX = 0;
    bool isGyroSet;

    void Start() {
        mainCamera = Camera.main;
    }

    void Update() {
        if (isGyroSet) {
            var rotation = Input.gyro.gravity.x;
            var diff = rotation - initialGyroPosX;
            var temp = transform.position;
            temp.x += diff * gyroSpeed;
            temp.x = Mathf.Clamp(temp.x, clampingPositions.x, clampingPositions.y);
            transform.position = temp;
        } else {
            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject()) {
                if (previousPos == Vector3.zero) {
                    previousPos = Input.mousePosition;
                }
                if (Input.mousePosition != previousPos) {
                    var mousePosition = mainCamera.ScreenToViewportPoint(Input.mousePosition - previousPos).x;
                    var temp = transform.position;
                    temp.x += mousePosition * speed;
                    temp.x = Mathf.Clamp(temp.x, clampingPositions.x, clampingPositions.y);
                    transform.position = temp;
                }
            }

            if (Input.GetMouseButtonUp(0)) {
                previousPos = Vector3.zero;
            }
        }
    }

    public void SetGyroInitialRotation() {
        if (GameController.playerData.isUsingGyro) {
            isGyroSet = true;
            Input.gyro.enabled = true;
            initialGyroPosX = Input.gyro.gravity.x;
        }
    }

    public void ResetGyro() {
        if (GameController.playerData.isUsingGyro) {
            isGyroSet = false;
            initialGyroPosX = 0;
        }
    }
}
