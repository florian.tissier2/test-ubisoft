﻿using UnityEngine;
using System.Collections;

public class CancelRotation : MonoBehaviour {

    [SerializeField] Transform rotationToCancel;

    // Update is called once per frame
    void Update() {
        var temp = transform.rotation;
        temp.eulerAngles = rotationToCancel.rotation.eulerAngles;
        transform.rotation = temp;
    }
}
