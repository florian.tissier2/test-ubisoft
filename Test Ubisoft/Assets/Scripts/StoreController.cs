﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;

public class StoreController : MonoBehaviour {
    [SerializeField] ScrollRect scrollRect;
    [SerializeField] RectTransform content;
    [SerializeField] Image ballsTab;
    [SerializeField] Image blocksTab;
    [SerializeField] Color activeTabColor;
    [SerializeField] Color inactiveTabColor;

    [Header("Balls")]
    [SerializeField] GameObject ballsContainer;
    [SerializeField] RectTransform socialLine;
    [SerializeField] RectTransform ballGemContainer;
    [SerializeField] RectTransform ballFlagContainer;
    [SerializeField] Text ballFlagAmount;
    [SerializeField] RectTransform ballVideoContainer;
    [SerializeField] Text ballVideoAmount;
    [SerializeField] RectTransform ballChallengeContainer;
    [SerializeField] Text ballChallengeAmount;

    [Header("Blocks")]
    [SerializeField] GameObject blocksContainer;
    [SerializeField] RectTransform blocksGemContainer;

    [Header("New Panel")]
    [SerializeField] GameObject newObjectPanel;
    [SerializeField] Text newObjectText;
    [SerializeField] Image newObjectImage;
    [SerializeField] CanvasGroup newObjectButton;

    public delegate void Purchase();
    public static event Purchase OnPurchase;

    GameObject shopItemPrefab;
    int numberOfBlocks = 24;
    bool isBallTabActive = false;
    Dictionary<string, ShopItem> ballsShopItem = new Dictionary<string, ShopItem>();
    Dictionary<string, ShopItem> blocksShopItem = new Dictionary<string, ShopItem>();

    void Start() {
        newObjectPanel.SetActive(false);
        shopItemPrefab = Resources.Load<GameObject>(Res.shopItemPrefab);
    }

    public IEnumerator SetStore(bool forBall = true) {
        isBallTabActive = forBall;
        ballsContainer.SetActive(forBall);
        blocksContainer.SetActive(!forBall);
        ballsTab.color = forBall ? activeTabColor : inactiveTabColor;
        blocksTab.color = !forBall ? activeTabColor : inactiveTabColor;

        if (forBall) {
            ballsShopItem.Clear();
            ballGemContainer.DeleteChildren();
            ballFlagContainer.DeleteChildren();
            ballVideoContainer.DeleteChildren();
            ballChallengeContainer.DeleteChildren();

            var ballRandom = Instantiate(shopItemPrefab, ballGemContainer, false);
            var ballRandomController = ballRandom.GetComponent<ShopItem>();
            ballRandomController.SetShopItem(ShopItemType.Random, 0, Resources.Load<Sprite>(Res.diceSprite), true, GameController.playerData.activeBall == "Random", () => onBallShopItemClicked("Random", ballRandomController));
            ballsShopItem.Add("Random", ballRandomController);

            var ballsList = GameController.playerData.gemBalls;
            for (int i = 0; i < ballsList.Length; i++) {
                var go = Instantiate(shopItemPrefab, ballGemContainer, false);
                var name = ballsList[i].nameSprite;
                var controller = go.GetComponent<ShopItem>();
                controller.SetShopItem(
                    ShopItemType.Gem,
                    ballsList[i].price,
                    Resources.Load<Sprite>(Res.spritesFolder + name),
                    ballsList[i].isDiscovered,
                    ballsList[i].isSelected,
                    () => onBallShopItemClicked(name, controller));
                ballsShopItem.Add(name, controller);
            }
            int numberOfBallsDiscovered = 0;
            ballsList = GameController.playerData.flagBalls;
            for (int i = 0; i < ballsList.Length; i++) {
                if (ballsList[i].isDiscovered)
                    numberOfBallsDiscovered++;
                var go = Instantiate(shopItemPrefab, ballFlagContainer, false);
                var name = ballsList[i].nameSprite;
                var controller = go.GetComponent<ShopItem>();
                controller.SetShopItem(
                    ShopItemType.Flag,
                    ballsList[i].price,
                    Resources.Load<Sprite>(Res.spritesFolder + name),
                    ballsList[i].isDiscovered,
                    ballsList[i].isSelected,
                    () => onBallShopItemClicked(name, controller));
                ballsShopItem.Add(name, controller);
            }
            ballFlagAmount.text = numberOfBallsDiscovered + "/" + ballsList.Length;

            /*numberOfBallsDiscovered = 0;
            ballsList = GameController.playerData.videoBalls;
            for (int i = 0; i < ballsList.Length; i++) {
                if (ballsList[i].isDiscovered)
                    numberOfBallsDiscovered++;
                var go = Instantiate(shopItemPrefab, ballVideoContainer, false);
                var name = ballsList[i].nameSprite;
                var controller = go.GetComponent<ShopItem>();
                controller.SetShopItem(
                    ShopItemType.Video,
                    ballsList[i].price,
                    Resources.Load<Sprite>(Res.spritesFolder + name),
                    ballsList[i].isDiscovered,
                    ballsList[i].isSelected,
                    () => onBallShopItemClicked(name, controller));
                ballsShopItem.Add(name, controller);
            }
            ballVideoAmount.text = numberOfBallsDiscovered + "/" + ballsList.Length;

            numberOfBallsDiscovered = 0;
            ballsList = GameController.playerData.challengeBalls;
            for (int i = 0; i < ballsList.Length; i++) {
                if (ballsList[i].isDiscovered)
                    numberOfBallsDiscovered++;
                var go = Instantiate(shopItemPrefab, ballChallengeContainer, false);
                var name = ballsList[i].nameSprite;
                var controller = go.GetComponent<ShopItem>();
                controller.SetShopItem(
                    ShopItemType.Challenge,
                    ballsList[i].price,
                    Resources.Load<Sprite>(Res.spritesFolder + name),
                    ballsList[i].isDiscovered,
                    ballsList[i].isSelected,
                    () => onBallShopItemClicked(name, controller));
                ballsShopItem.Add(name, controller);
            }
            ballChallengeAmount.text = numberOfBallsDiscovered + "/" + ballsList.Length*/

            LayoutRebuilder.ForceRebuildLayoutImmediate(ballsContainer.GetComponent<RectTransform>());
        } else {
            blocksShopItem.Clear();
            blocksGemContainer.DeleteChildren();
            var blockRandom = Instantiate(shopItemPrefab, blocksGemContainer, false);
            var blockRandomController = blockRandom.GetComponent<ShopItem>();
            blockRandomController.SetShopItem(ShopItemType.Random, 0, Resources.Load<Sprite>(Res.diceSprite), true, GameController.playerData.activeBlock == "Random", () => onBlockShopItemClicked("Random", blockRandomController));
            blocksShopItem.Add("Random", blockRandomController);

            var blocksList = GameController.playerData.blocks;
            for (int i = 0; i < blocksList.Length; i++) {
                var go = Instantiate(shopItemPrefab, blocksGemContainer, false);
                var controller = go.GetComponent<ShopItem>();
                var name = blocksList[i].nameSprite;
                controller.SetShopItem(
                    ShopItemType.Gem,
                    blocksList[i].price,
                    Resources.Load<Sprite>(Res.spritesFolder + name),
                    blocksList[i].isDiscovered,
                    blocksList[i].isSelected,
                    () => onBlockShopItemClicked(name, controller));
                blocksShopItem.Add(blocksList[i].nameSprite, controller);
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(blocksContainer.GetComponent<RectTransform>());
        }

        yield return null;

        var temp = content.sizeDelta;
        if (forBall) {
            // content size fitter won't work on Content so I have to use this formula to get its height
            temp.y = ballGemContainer.sizeDelta.y +
                ballFlagContainer.sizeDelta.y +
                /*ballVideoContainer.sizeDelta.y +
                ballChallengeContainer.sizeDelta.y +*/
                3 * 50 +//7 * 50 + // for the spacing
                60 + //3 * 60 + // for the dividers
                socialLine.sizeDelta.y; // the social line
        } else {
            temp.y = blocksGemContainer.sizeDelta.y;
        }
        content.sizeDelta = temp;

        scrollRect.verticalNormalizedPosition = 1;
    }

    public void OnBallsTabClicked() {
        if (!isBallTabActive)
            StartCoroutine(SetStore());
    }

    public void OnBlocksTabClicked() {
        if (isBallTabActive)
            StartCoroutine(SetStore(false));
    }

    void onBallShopItemClicked(string name, ShopItem shopItem) {
        switch (shopItem.type) {
            case ShopItemType.Random:
                setNewBall(name, shopItem);
                break;
            case ShopItemType.Flag:
            case ShopItemType.Gem:
                if (shopItem.isDiscovered) {
                    if (shopItem.isSelected) {
                        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().HideStorePanel();
                    } else {
                        setNewBall(name, shopItem);
                    }
                } else {
                    if (GameController.playerData.diamondCount >= shopItem.price) {
                        GameController.playerData.diamondCount -= shopItem.price;
                        OnPurchase?.Invoke();
                        openNewObjectPanel(shopItem.sprite);
                        setNewBall(name, shopItem);
                    }
                }
                break;
            /*case ShopItemType.Video:
                break;
            case ShopItemType.Challenge:
                break;*/
            default:
                break;
        }
    }

    void setNewBall(string name, ShopItem shopItem) {
        GameController.playerData.activeBall = name;

        shopItem.Select();
        foreach (var keyValuePair in ballsShopItem) {
            if (keyValuePair.Key != name) {
                keyValuePair.Value.Unselect();
            }
        }

        foreach (var shopObject in GameController.playerData.gemBalls) {
            shopObject.isSelected = shopObject.nameSprite == name;
            if (shopObject.nameSprite == name) {
                shopObject.isDiscovered = true;
            }
        }
        foreach (var shopObject in GameController.playerData.flagBalls) {
            shopObject.isSelected = shopObject.nameSprite == name;
            if (shopObject.nameSprite == name) {
                shopObject.isDiscovered = true;
            }
        }
        /*foreach (var shopObject in GameController.playerData.videoBalls) {
            shopObject.isSelected = shopObject.nameSprite == name;
            if (shopObject.nameSprite == name) {
                shopObject.isDiscovered = true;
            }
        }
        foreach (var shopObject in GameController.playerData.challengeBalls) {
            shopObject.isSelected = shopObject.nameSprite == name;
            if (shopObject.nameSprite == name) {
                shopObject.isDiscovered = true;
            }
        }*/

        DataManager.SavePlayerData(GameController.playerData);
    }

    void onBlockShopItemClicked(string name, ShopItem shopItem) {
        if (shopItem.isDiscovered) {
            if (shopItem.isSelected) {
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().HideStorePanel();
            } else {
                setNewBlock(name, shopItem);
            }
        } else {
            if (GameController.playerData.diamondCount > shopItem.price) {
                GameController.playerData.diamondCount -= shopItem.price;
                OnPurchase?.Invoke();
                openNewObjectPanel(shopItem.sprite, false);
                setNewBlock(name, shopItem);
            }
        }
    }

    void setNewBlock(string name, ShopItem shopItem) {
        GameController.playerData.activeBlock = name;

        shopItem.Select();
        foreach (var keyValuePair in blocksShopItem) {
            if (keyValuePair.Key != name) {
                keyValuePair.Value.Unselect();
            }
        }

        foreach (var shopObject in GameController.playerData.blocks) {
            shopObject.isSelected = shopObject.nameSprite == name;
            if (shopObject.nameSprite == name) {
                shopObject.isDiscovered = true;
            }
        }

        DataManager.SavePlayerData(GameController.playerData);
    }

    void openNewObjectPanel(Sprite sprite, bool forBall = true) {
        newObjectText.GetComponent<RectTransform>().DOAnchorPosY(700, 0);
        newObjectText.DOFade(0, 0);
        newObjectText.text = forBall ? "NEW BALL" : "NEW BLOCK";
        newObjectImage.transform.DOScale(0, 0);
        newObjectImage.sprite = sprite;
        newObjectButton.DOFade(0, 0);
        newObjectButton.GetComponent<RectTransform>().DOAnchorPosY(-750, 0);

        var sequence = DOTween.Sequence()
            .Insert(0, newObjectText.GetComponent<RectTransform>().DOAnchorPosY(700, 0.3f))
            .Insert(0.1f, newObjectText.DOFade(1, 0.2f))
            .Insert(0, newObjectButton.GetComponent<RectTransform>().DOAnchorPosY(-750, 0.3f))
            .Insert(0.1f, newObjectButton.DOFade(1, 0.2f))
            .Insert(0.2f, newObjectImage.transform.DOScale(1, 0.3f))
            .OnStart(() => {
                newObjectPanel.SetActive(true);
                AudioManager.PlaySfx(Res.newPurchaseSfx);
            })
            ;
    }

    public void CloseNewObjectPanel() {
        newObjectPanel.SetActive(false);
    }
}
