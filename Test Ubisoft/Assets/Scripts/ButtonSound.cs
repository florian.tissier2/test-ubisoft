﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour {

    [SerializeField] AudioClip sound;
    
    void Start() {
        GetComponent<Button>().onClick.AddListener(() => AudioManager.PlaySfx(sound));
    }
}
