﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Res 
{
    const string prefabsFolder = "Prefabs/";
    const string materialsFolder = "Materials/";
    const string audioFolder = "Audio/";

    //Platforms
    const string platformsFolder = prefabsFolder + "Platforms/";
    public const string basicPlatformsFolder = platformsFolder + "Basic/";
    public const string endingBoardPlatformsFolder = platformsFolder + "EndingBoard/";
    public const string middleBoardPlatformsFolder = platformsFolder + "MiddleBoard/";
    public const string speedBoostPlatformsFolder = platformsFolder + "SpeedBoost/";

    //Misc
    public const string diamondPrefab = prefabsFolder + "Diamond";
    public const string shopItemPrefab = prefabsFolder + "ShopItem";
    public const string ripplePrefab = prefabsFolder + "Ripple";
    public const string wallsPrefab = prefabsFolder + "Walls";

    //Sprites
    public const string spritesFolder = "Sprites/";
    public const string diceSprite = spritesFolder + "reddice";

    //Materials
    public const string platformMaterialsFolder = materialsFolder + "Platforms/";
    public const string ballMaterialsFolder = materialsFolder + "Balls/";

    //Audio
    public const string sfxSourcePrefab = audioFolder + "SfxSource";
    public const string musicSourcePrefab = audioFolder + "MusicSource";
    public const string landingSfx = audioFolder + "landing";
    public const string perfectLandingSfx = audioFolder + "perfectLanding";
    public const string speedboostSfx = audioFolder + "speedboost";
    public const string newPurchaseSfx = audioFolder + "newPurchase";
    public const string diamondSfx = audioFolder + "diamond";
}
