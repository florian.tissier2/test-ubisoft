using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Dweiss;
using Cinemachine;

public class Ball : MonoBehaviour {
    public delegate void AddPoints(int points);
    public static event AddPoints OnPerfectLanding;
    public static event AddPoints OnFlyingTime;
    public delegate void Landing(bool rightSide, int landingPoints, int flyingPoints);
    public static event Landing OnLanding;
    public delegate void StopPerfectLanding();
    public static event StopPerfectLanding OnStopPerfectLanding;
    public delegate void Loose();
    public static event Loose OnLoose;
    public delegate void State();
    public static event State IsGrounded;
    public static event State IsFlying;
    public static event State SpeedIncrease;
    public static event State SpeedDecrease;

    [SerializeField] HorizontalMove horizontalMoveScript;
    [SerializeField] private float initialSpeed = 30;
    [SerializeField] private float maxSpeed = 300;
    [SerializeField] private float timeToDecreaseSpeed = 2;
    [SerializeField] private float fallMultiplier = 8;
    [SerializeField] Renderer meshRenderer;
    [SerializeField] TrailRenderer trail;
    [SerializeField] ParticleSystem smoke;
    [SerializeField] AudioSource smokeSound;
    [SerializeField] ParticleSystem fire;
    [SerializeField] Material fireMaterial;
    [SerializeField] AudioSource fireSound;

    Rigidbody rb;
    LevelGenerator levelGenerator;
    Material originalMaterial;
    GameObject ripplePrefab;

    bool canMove = false, firstMove = true, isDead = false;
    bool isGrounded = false, hasTouchedFirstPlatform = false;
    float currentSpeed, landingSpeed, boostSpeed, boostWhenDecreasingStarts;
    bool decreaseSpeed = false;
    float decreaseCounter = 0;
    Coroutine decreaseSpeedCoroutine;
    float timeNotGrounded, thresholdTimeNotGrounded = 0.5f;
    Vector3 initialPosition;
    int perfectLandingCombo = 0;
    int scoreCombo = 0;
    float flyingScore = 0;

    Vector3 storedVelocity = Vector3.zero, storedAngularVelocity = Vector3.zero;

    CinemachineVirtualCamera vcam;

    void Start() {
        horizontalMoveScript.enabled = false;
        initialPosition = transform.position;
        levelGenerator = GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelGenerator>();
        var activeBall = GameController.playerData.activeBall;
        if (GameController.playerData.activeBall == "Random") {
            activeBall = getRandomMaterial();
        }
        meshRenderer.material = Resources.Load<Material>(Res.ballMaterialsFolder + activeBall);
        ripplePrefab = Resources.Load<GameObject>(Res.ripplePrefab);
        originalMaterial = meshRenderer.material;
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        currentSpeed = initialSpeed;
        StopMovingBall();
    }

    public void StartMovingBall(CinemachineVirtualCamera cam) {
        vcam = cam;
        canMove = true;
        rb.useGravity = true;
        rb.velocity = storedVelocity;
        rb.angularVelocity = storedAngularVelocity;
        horizontalMoveScript.enabled = true;
        horizontalMoveScript.SetGyroInitialRotation();
        if (firstMove) {
            rb.AddForce(Vector3.forward * 50);
            firstMove = false;
        }
    }

    public void StopMovingBall() {
        canMove = false;
        rb.useGravity = false;
        storedVelocity = rb.velocity;
        rb.velocity = Vector3.zero;
        storedAngularVelocity = rb.angularVelocity;
        rb.angularVelocity = Vector3.zero;
        horizontalMoveScript.ResetGyro();
        horizontalMoveScript.enabled = false;
    }

    public void ResetBallPosition(Vector3 position) {
        StopMovingBall();
        storedVelocity = Vector3.zero;
        storedAngularVelocity = Vector3.zero;
        currentSpeed = initialSpeed;
        perfectLandingCombo = 0;
        var temp = position + initialPosition;
        temp.x = 0;
        transform.position = temp;
        isDead = false;
        firstMove = true;
        hasTouchedFirstPlatform = false;
    }

    void Update() {
        if (canMove) {
            if (decreaseSpeed) {
                float percentageComplete = decreaseCounter / timeToDecreaseSpeed;
                boostSpeed = Mathf.Lerp(boostWhenDecreasingStarts, 0, percentageComplete);
                decreaseCounter += Time.deltaTime;

                if (percentageComplete >= 1) {
                    decreaseSpeed = false;
                }
            }

            currentSpeed = initialSpeed + landingSpeed + boostSpeed;
            if (currentSpeed > maxSpeed) {
                currentSpeed = maxSpeed;
            }

            if (!isGrounded) {
                //little trick to have a better jump feeling, found at https://www.youtube.com/watch?v=7KiK0Aqtmzc
                if (rb.velocity.y < 0) {// is falling
                    rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
                } else {// is ascending
                        // if ball is still ascending after thresholdTimeNotGrounded seconds, add force to make it fall 
                        // so it don't spend too much time in the air
                    if (timeNotGrounded >= thresholdTimeNotGrounded) {
                        rb.AddForce(Vector3.down * 100);
                    }
                    timeNotGrounded += Time.deltaTime;
                }
                if (hasTouchedFirstPlatform) {
                    flyingScore += (Time.deltaTime * 100);
                    OnFlyingTime?.Invoke(Mathf.FloorToInt(flyingScore));
                }

                if (!isDead) {
                    var closestPlatformY = levelGenerator.GetClosestPlatformY();
                    var zBounds = levelGenerator.GetClosestPlatformZBounds();
                    //check if we missed the closest platform
                    if (closestPlatformY > transform.position.y + 1 && (transform.position.z < zBounds.y)) {
                        bool trajectoryTouchPlatform = false;

                        //we then calculate trajectory to see if, even if we missed a platform, we can touch another one
                        var res = rb.CalculateMovement(50, 0.1f);
                        for (int i = 0; i < res.Length; ++i) {
                            if (Physics.Raycast(res[i], Vector3.down, 100)) {
                                trajectoryTouchPlatform = true;
                                break;
                            }
                        }
                        if (!trajectoryTouchPlatform) {
                            die();
                        }
                    }
                }
            }
        }
    }

    void die() {
        stopEffects();
        isDead = true;
        OnLoose?.Invoke();
        rb.AddForce(Vector3.down * 1000);
        StartCoroutine(stopMovingAfter(1));
        horizontalMoveScript.enabled = false;
    }

    void FixedUpdate() {
        if (canMove) {
            rb.AddForce(Vector3.forward * (currentSpeed - rb.velocity.z)); // to clamp the velocity at currentSpeed
        }
    }

    public void OnCollisionEnter(Collision collision) {
        if (collision.collider.tag == "Ground" && !isGrounded) {
            isGrounded = true;
            IsGrounded?.Invoke();
            if (!hasTouchedFirstPlatform) {
                hasTouchedFirstPlatform = true;
            }
            Instantiate(ripplePrefab, collision.GetContact(0).point, Quaternion.identity);
            var platformCenter = collision.collider.transform.position.x;
            //0.5 makes the perfect landing zone 1 in size (0.5 on each side of the center)
            var diff = collision.GetContact(0).point.x - platformCenter;
            var diffAbs = Mathf.Abs(diff);

            if (diffAbs <= 2.5f) {
                var landingPoints = Mathf.FloorToInt(diffAbs.remap(2.5f, 0, 50, 200));
                OnLanding?.Invoke(diff > 0, landingPoints, Mathf.FloorToInt(flyingScore));
                flyingScore = 0;
                if (diffAbs <= 0.5f) {
                    SpeedIncrease?.Invoke();
                    AudioManager.PlaySfx(Res.perfectLandingSfx);
                    perfectLandingCombo++;
                    scoreCombo += perfectLandingCombo * 100;
                    OnPerfectLanding?.Invoke(scoreCombo);
                    landingSpeed += 20;
                    if (perfectLandingCombo == 1) {
                        trail.enabled = true;
                    } else if (perfectLandingCombo == 2) {
                        trail.enabled = false;
                        smoke.Play();
                        if (!smokeSound.isPlaying)
                            smokeSound.Play();
                    } else {
                        trail.enabled = false;
                        smoke.Stop();
                        smokeSound.Stop();
                        fire.Play();
                        if (!fireSound.isPlaying)
                            fireSound.Play();
                        meshRenderer.material = fireMaterial;
                    }
                } else {
                    SpeedDecrease?.Invoke();
                    AudioManager.PlaySfx(Res.landingSfx);
                    OnStopPerfectLanding?.Invoke();
                    perfectLandingCombo = 0;
                    scoreCombo = 0;
                    landingSpeed = 0;
                    stopEffects();
                }
            }
        }
    }

    public void OnCollisionExit(Collision collision) {
        if (collision.collider.tag == "Ground") {
            isGrounded = false;
            IsFlying?.Invoke();
            timeNotGrounded = 0;
        }
    }

    public void OnTriggerEnter(Collider other) {
        if (other.tag == "SpeedBoost") {
            AudioManager.PlaySfx(Res.speedboostSfx);
            boostSpeed += 150;
            decreaseBoost();
        }

        if (other.tag == "Springboard") {
            AudioManager.PlaySfx(Res.speedboostSfx);
            boostSpeed += 50;
            decreaseBoost();
        }

        if (boostSpeed > 250)
            boostSpeed = 250;
    }

    void decreaseBoost() {
        decreaseCounter = 0;
        boostWhenDecreasingStarts = boostSpeed;
        if (decreaseSpeedCoroutine != null) {
            StopCoroutine(decreaseSpeedCoroutine);
        }
        decreaseSpeedCoroutine = StartCoroutine(startDecreasingSpeedAfter(0.5f));
    }

    void stopEffects() {
        trail.enabled = false;
        smoke.Stop();
        smokeSound.Stop();
        fire.Stop();
        fireSound.Stop();
        meshRenderer.material = originalMaterial;
    }

    IEnumerator startDecreasingSpeedAfter(float seconds) {
        yield return new WaitForSeconds(seconds);
        boostWhenDecreasingStarts = boostSpeed;
        decreaseCounter = 0;
        decreaseSpeed = true;
    }

    IEnumerator stopMovingAfter(float seconds) {
        yield return new WaitForSeconds(seconds);
        StopMovingBall();
    }

    string getRandomMaterial() {
        var listAvailable = new List<string>();

        foreach (var shopObject in GameController.playerData.gemBalls) {
            if (shopObject.isDiscovered) {
                listAvailable.Add(shopObject.nameSprite);
            }
        }
        foreach (var shopObject in GameController.playerData.flagBalls) {
            if (shopObject.isDiscovered) {
                listAvailable.Add(shopObject.nameSprite);
            }
        }
        foreach (var shopObject in GameController.playerData.videoBalls) {
            if (shopObject.isDiscovered) {
                listAvailable.Add(shopObject.nameSprite);
            }
        }
        foreach (var shopObject in GameController.playerData.challengeBalls) {
            if (shopObject.isDiscovered) {
                listAvailable.Add(shopObject.nameSprite);
            }
        }

        return listAvailable[Random.Range(0, listAvailable.Count - 1)];
    }

    public bool IsDead() {
        return isDead;
    }
}
