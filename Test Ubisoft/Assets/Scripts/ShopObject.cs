﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ShopObject {
    public int price;
    public string nameSprite;
    public bool isDiscovered;
    public bool isSelected;
}
