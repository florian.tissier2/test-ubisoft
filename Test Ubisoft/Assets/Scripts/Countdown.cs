﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Countdown : MonoBehaviour {
    [SerializeField] Animator animator;
    [SerializeField] Button button;
    [SerializeField] Text counter;
    [SerializeField] double time;
    [SerializeField] AudioClip clip;

    public void Update() {
        var diff = GameController.playerData.lastGiftTime.AddMinutes(time) - DateTime.Now;
        if (diff.Hours <= 0 && diff.Minutes <= 0 && diff.Seconds <= 0) {
            counter.text = "READY";
            animator.enabled = true;
            button.enabled = true;
        } else {
            counter.text = diff.ToString(@"mm\:ss");
            button.enabled = false;
            animator.enabled = false;
        }
    }

    public void PlaySound() {
        AudioManager.PlaySfx(clip);
    }
}
