﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class PlayerData {
    public int highScore;
    public int diamondCount;
    public ShopObject[] gemBalls;
    public ShopObject[] flagBalls;
    public ShopObject[] videoBalls;
    public ShopObject[] challengeBalls;
    public ShopObject[] blocks;
    public string activeBall;
    public string activeBlock;
    public bool canPlayMusic;
    public bool canPlaySfx;
    public DateTime lastGiftTime;
    public bool gotFacebookGift;
    public bool gotTwitterGift;
    public bool gotInstagramGift;
    public bool isUsingGyro;

    public PlayerData(int highScore, int diamondCount, ShopObject[] gemBalls, ShopObject[] flagBalls, ShopObject[] videoBalls, ShopObject[] challengeBalls, ShopObject[] blocks, string activeBall, string activeBlock, bool canPlayMusic, bool canPlaySfx, DateTime lastGiftTime, bool gotFacebookGift, bool gotTwitterGift, bool gotInstagramGift, bool isUsingGyro) {
        this.highScore = highScore;
        this.diamondCount = diamondCount;
        this.gemBalls = gemBalls;
        this.flagBalls = flagBalls;
        this.videoBalls = videoBalls;
        this.challengeBalls = challengeBalls;
        this.blocks = blocks;
        this.activeBall = activeBall;
        this.activeBlock = activeBlock;
        this.canPlayMusic = canPlayMusic;
        this.canPlaySfx = canPlaySfx;
        this.lastGiftTime = lastGiftTime;
        this.gotFacebookGift = gotFacebookGift;
        this.gotTwitterGift = gotTwitterGift;
        this.gotInstagramGift = gotInstagramGift;
        this.isUsingGyro = isUsingGyro;
    }
}
