﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public static class AudioManager {

    static AudioSource sfxSource1, sfxSource2, musicSource;
    static Dictionary<string, AudioClip> loadedSounds;
    static int lastSourcePlaying;

    public static void Initialize() {
        loadedSounds = new Dictionary<string, AudioClip>();
        sfxSource1 = GameObject.Instantiate(Resources.Load<GameObject>(Res.sfxSourcePrefab)).GetComponent<AudioSource>();
        sfxSource2 = GameObject.Instantiate(Resources.Load<GameObject>(Res.sfxSourcePrefab)).GetComponent<AudioSource>();
        musicSource = GameObject.Instantiate(Resources.Load<GameObject>(Res.musicSourcePrefab)).GetComponent<AudioSource>();
    }

    public static void PlaySfx(string soundName) {
        if (GameController.playerData.canPlaySfx) {
            AudioClip sound;
            if (loadedSounds.ContainsKey(soundName)) {
                sound = loadedSounds[soundName];
            } else {
                sound = Resources.Load<AudioClip>(soundName);
            }
            PlaySfx(sound);
        }
    }

    public static void PlaySfx(AudioClip sound) {
        if (GameController.playerData.canPlaySfx) {
            if (!sfxSource1.isPlaying) {
                lastSourcePlaying = 1;
                sfxSource1.PlayOneShot(sound);
            } else if (!sfxSource2.isPlaying) {
                lastSourcePlaying = 2;
                sfxSource2.PlayOneShot(sound);
            } else { // Si quelque chose play sur les 2 sources, on regarde celle qui a eu un son à jouer en dernier et on play le son sur l'autre
                if (lastSourcePlaying == 1) {
                    lastSourcePlaying = 2;
                    sfxSource2.PlayOneShot(sound);
                } else {
                    lastSourcePlaying = 1;
                    sfxSource1.PlayOneShot(sound);
                }
            }
        }
    }

    public static void PlayMusic() {
        if (GameController.playerData.canPlayMusic) {
            if (!musicSource.isPlaying) {
                musicSource.DOFade(0, 0);
                musicSource.DOFade(1, 1).OnStart(() => musicSource.Play());
            }
        }
    }

    public static void StopMusic() {
        musicSource.DOFade(0, 1).OnComplete(() => musicSource.Stop());
    }

    static void stopMusicNow() {
        musicSource.Stop();
    }

    public static void MusicStateChanged() {
        if (GameController.playerData.canPlayMusic) {
            PlayMusic();
        } else {
            stopMusicNow();
        }
    }
}
