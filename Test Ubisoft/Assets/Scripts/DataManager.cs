﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;

public static class DataManager {

    static string highScoreKey = "HighScore";
    static string diamondCountKey = "DiamondCount";
    static string gemBallsKey = "GemBalls";
    static string flagBallsKey = "FlagBalls";
    static string videoBallsKey = "videoBalls";
    static string challengeBallsKey = "ChallengeBalls";
    static string blocksKey = "Blocks";
    static string activeBallKey = "ActiveBall";
    static string activeBlockKey = "ActiveBlock";
    static string sfxOptionkKey = "SfxOption";
    static string musicOptionKey = "MusicOption";
    static string lastGiftTimeKey = "LastGiftTime";
    static string gotFacebookGiftKey = "GotFacebookGift";
    static string gotTwitterGiftKey = "GotTwitterGift";
    static string gotInstagramGiftKey = "GotInstagramGift";
    static string isUsingGyroKey = "IsUsingGyro";


    public static PlayerData LoadPlayerData() {
        var highScore = PlayerPrefs.GetInt(highScoreKey, 0);
        var diamondCount = PlayerPrefs.GetInt(diamondCountKey, 0);
        var activeBall = PlayerPrefs.GetString(activeBallKey, "ball1");
        var activeBlock = PlayerPrefs.GetString(activeBlockKey, "block1");
        var canPlayMusic = PlayerPrefs.GetInt(musicOptionKey, 1) == 1;
        var canPlaySfx = PlayerPrefs.GetInt(sfxOptionkKey, 1) == 1;
        var lastGiftTimeString = PlayerPrefs.GetString(lastGiftTimeKey, "");
        var gotFacebookGift = PlayerPrefs.GetInt(gotFacebookGiftKey, 0) == 1;
        var gotTwitterGift = PlayerPrefs.GetInt(gotTwitterGiftKey, 0) == 1;
        var gotInstagramGift = PlayerPrefs.GetInt(gotInstagramGiftKey, 0) == 1;
        var isUsingGyro = PlayerPrefs.GetInt(isUsingGyroKey, 0) == 1;
        isUsingGyro = isUsingGyro && SystemInfo.supportsGyroscope;

        DateTime lastGiftTime;
        if(lastGiftTimeString == "") {
            lastGiftTime = DateTime.Now;
        } else {
            lastGiftTime = DateTime.Parse(lastGiftTimeString);
        }

        var gemBallsJson = PlayerPrefs.GetString(gemBallsKey, "");
        var gemBallsList = new ShopObject[] { };
        if (gemBallsJson != "") {
            gemBallsList = decodeFromJSON(gemBallsJson);
        } else {
            gemBallsList = createGemBallsList();
        }

        var flagBallsJson = PlayerPrefs.GetString(flagBallsKey, "");
        var flagBallsList = new ShopObject[] { };
        if (flagBallsJson != "") {
            flagBallsList = decodeFromJSON(flagBallsJson);
        } else {
            flagBallsList = createFlagBallsList();
        }

        /*var videoBallsJson = PlayerPrefs.GetString(videoBallsKey, "");
        var videoBallsList = new ShopObject[] { };
        if (videoBallsJson != "") {
            videoBallsList = decodeFromJSON(videoBallsJson);
        } else {*/
            var videoBallsList = createVideoBallsList();
        //}

        /*var challengeBallsJson = PlayerPrefs.GetString(challengeBallsKey, "");
        var challengeBallsList = new ShopObject[] { };
        if (challengeBallsJson != "") {
            challengeBallsList = decodeFromJSON(challengeBallsJson);
        } else {*/
            var challengeBallsList = createChallengeBallsList();
        //}

        var blocksJson = PlayerPrefs.GetString(blocksKey, "");
        var blocksList = new ShopObject[] { };
        if (blocksJson != "") {
            blocksList = decodeFromJSON(blocksJson);
        } else {
            blocksList = createBlocksList();
        }

        return new PlayerData(highScore, diamondCount, gemBallsList, flagBallsList, videoBallsList, challengeBallsList, blocksList, activeBall, activeBlock, 
            canPlayMusic, canPlaySfx, lastGiftTime, gotFacebookGift, gotTwitterGift, gotInstagramGift, isUsingGyro);
    }

    public static void SavePlayerData(PlayerData data) {
        PlayerPrefs.SetInt(highScoreKey, data.highScore);
        PlayerPrefs.SetInt(diamondCountKey, data.diamondCount);
        PlayerPrefs.SetString(activeBallKey, data.activeBall);
        PlayerPrefs.SetString(activeBlockKey, data.activeBlock);
        PlayerPrefs.SetString(gemBallsKey, encodeToJSON(data.gemBalls));
        PlayerPrefs.SetString(flagBallsKey, encodeToJSON(data.flagBalls));
        PlayerPrefs.SetString(videoBallsKey, encodeToJSON(data.videoBalls));
        PlayerPrefs.SetString(challengeBallsKey, encodeToJSON(data.challengeBalls));
        PlayerPrefs.SetString(blocksKey, encodeToJSON(data.blocks));
        PlayerPrefs.SetInt(musicOptionKey, data.canPlayMusic ? 1 : 0);
        PlayerPrefs.SetInt(sfxOptionkKey, data.canPlaySfx ? 1 : 0);
        PlayerPrefs.SetString(lastGiftTimeKey, data.lastGiftTime.ToString());
        PlayerPrefs.SetInt(gotFacebookGiftKey, data.gotFacebookGift ? 1 : 0);
        PlayerPrefs.SetInt(gotTwitterGiftKey, data.gotTwitterGift ? 1 : 0);
        PlayerPrefs.SetInt(gotInstagramGiftKey, data.gotInstagramGift ? 1 : 0);
        PlayerPrefs.SetInt(isUsingGyroKey, data.isUsingGyro ? 1 : 0);
    }

    public static void ClearAll() {
        PlayerPrefs.DeleteAll();
    }

    static string encodeToJSON(ShopObject[] list) {
        return JsonConvert.SerializeObject(list);
    }

    static ShopObject[] decodeFromJSON(string json) {
        return JsonConvert.DeserializeObject<ShopObject[]>(json);
    }

    static ShopObject[] createGemBallsList() {
        return new ShopObject[] {
            new ShopObject{ price = 200, nameSprite = "ball1", isDiscovered = true, isSelected = true },
            new ShopObject{ price = 200, nameSprite = "ball2", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball3", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball4", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball5", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball6", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball7", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball8", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball9", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball10", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "ball11", isDiscovered = false, isSelected = false }
        };
    }

    static ShopObject[] createFlagBallsList() {
        return new ShopObject[] {
            new ShopObject{ price = 200, nameSprite = "flagball1", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "flagball2", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "flagball3", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "flagball4", isDiscovered = false, isSelected = false }
        };
    }

    static ShopObject[] createVideoBallsList() {
        return new ShopObject[] {
        };
    }

    static ShopObject[] createChallengeBallsList() {
        return new ShopObject[] {
        };
    }

    static ShopObject[] createBlocksList() {
        return new ShopObject[] {
            new ShopObject{ price = 200, nameSprite = "block1", isDiscovered = true, isSelected = true },
            new ShopObject{ price = 200, nameSprite = "block2", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block3", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block4", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block5", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block6", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block7", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block8", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block9", isDiscovered = false, isSelected = false },
            new ShopObject{ price = 200, nameSprite = "block10", isDiscovered = false, isSelected = false }
        };
    }
}
