﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowController : MonoBehaviour
{
    [SerializeField] Transform objectToFollow;

    Vector3 diffPosition;

    public void InitToFollow(Transform obj, bool resetToContinue) {
        if (objectToFollow == null)
            objectToFollow = obj;
        if(!resetToContinue)
            diffPosition = transform.position - objectToFollow.transform.position;
    }

    void Update()
    {
        if (objectToFollow != null) {
            var tempPos = objectToFollow.transform.position + diffPosition;
            tempPos.x = transform.position.x;
            transform.position = tempPos;
        }
    }

    public void StopFollowing() {
        objectToFollow = null;
    }
}
