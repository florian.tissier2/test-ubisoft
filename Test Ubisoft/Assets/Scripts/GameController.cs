using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class GameController : MonoBehaviour {
    [SerializeField] CinemachineVirtualCamera vcam;
    [SerializeField] ParticleSystem speedLinesParticles;
    [SerializeField] GameObject ballPrefab;
    [SerializeField] LevelGenerator levelGenerator;
    [SerializeField] StoreController storeController;

    [Header("UI")]
    [SerializeField] GameObject loader;
    [SerializeField] GameObject ketchAd;
    [SerializeField] GameObject background;
    [SerializeField] GameObject pointsPanel;
    [SerializeField] CanvasGroup pointsCanvas;
    [SerializeField] Text pointsCounter;
    [SerializeField] Text flyingTimeCounter;
    [SerializeField] Text perfectLandingCounter;
    [SerializeField] Text landingRightCounter;
    [SerializeField] Text landingLeftCounter;
    [SerializeField] Image pauseButton;
    [SerializeField] CanvasGroup diamondCanvas;
    [SerializeField] Text diamondCounter;

    [Header("Start Menu")]
    [SerializeField] GameObject startPanel;
    [SerializeField] RectTransform logoRect;
    [SerializeField] Image logo;
    [SerializeField] RectTransform highscorePanel;
    [SerializeField] CanvasGroup highscoreCanvas;
    [SerializeField] Text highscore;
    [SerializeField] RectTransform buttonsPanelTransform;
    [SerializeField] CanvasGroup buttonsPanelCanvas;
    [SerializeField] CanvasGroup blueButtonsCanvas;
    [SerializeField] RectTransform[] blueButtonsTransform;

    [Header("Tutorial")]
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] CanvasGroup tutorialCanvas;
    [SerializeField] GameObject fingerTutorial;
    [SerializeField] GameObject gyroTutorial;

    [Header("Pause Menu")]
    [SerializeField] GameObject pausePanel;
    [SerializeField] Text pauseText;
    [SerializeField] GameObject pauseButtonsContainer;
    [SerializeField] RectTransform[] pauseButtonsTransform;
    [SerializeField] RectTransform settingsButtonTransform;

    [Header("Game Over Menu")]
    [SerializeField] GameObject gameOverPanel;
    [SerializeField] ContinueCountdown continueCountdown;
    [SerializeField] RectTransform gameOverContinueContainer;
    [SerializeField] CanvasGroup giftButton;
    [SerializeField] RectTransform gameOverHighScoreTransform;
    [SerializeField] Text gameOverHighScore;
    [SerializeField] RectTransform gameOverButtonsPanelTransform;
    [SerializeField] CanvasGroup gameOverBlueButtonsCanvas;
    [SerializeField] RectTransform[] gameOverblueButtonsTransform;
    //[SerializeField] RectTransform gameOverChallengeContainer;

    [Header("Store")]
    [SerializeField] GameObject storePanel;
    [SerializeField] RectTransform storeTopTransform;
    [SerializeField] RectTransform watchAdsButton;
    [SerializeField] GameObject storeMiddle;
    [SerializeField] RectTransform storeBottomTransform;

    [Header("Settings Menu")]
    [SerializeField] GameObject settingsPanel;
    [SerializeField] RectTransform settingsTopTransform;
    [SerializeField] GameObject settingsBottom;
    [SerializeField] GameObject settingsContainer;
    [SerializeField] GameObject settingsGPlayButton;
    [SerializeField] GameObject settingsAds;
    [SerializeField] GameObject settingsStudioLogo;
    [SerializeField] GameObject settingsSocialContainer;
    [SerializeField] RectTransform[] settingsSocialButtons;
    [SerializeField] RectTransform socialRewardsTransform;

    [Header("New Diamonds")]
    [SerializeField] GameObject newDiamondsPanel;
    [SerializeField] Text newDiamondsValue;
    [SerializeField] Image newDiamondsGift;
    [SerializeField] CanvasGroup newDiamondsContainer;

    int totalPoints = 0;
    bool isStarted = false, isPaused = false, isGameOver = false;
    GameObject ball;

    public static PlayerData playerData;

    public void OnEnable() {
        Ball.OnPerfectLanding += addPointsPerfectLanding;
        Ball.OnLanding += addPointsLanding;
        Ball.OnFlyingTime += onFlying;
        Ball.OnLoose += gameOver;
        Ball.SpeedIncrease += increaseSpeedLines;
        Ball.SpeedDecrease += stopSpeedLines;
        Diamond.OnDiamondCaught += onDiamondCaught;
        StoreController.OnPurchase += updateDiamondCounter;
        SettingsController.OnDisplayGift += GiftDiamonds;

        //DataManager.ClearAll();
        playerData = DataManager.LoadPlayerData();
        //playerData.diamondCount = 2000;
        AudioManager.Initialize();
        AdsManager.InitAds();
        background.SetActive(true);
        hideUI();
    }

    public void OnDisable() {
        Ball.OnPerfectLanding -= addPointsPerfectLanding;
        Ball.OnLanding += addPointsLanding;
        Ball.OnFlyingTime += onFlying;
        Ball.OnLoose -= gameOver;
        Ball.SpeedIncrease -= increaseSpeedLines;
        Ball.SpeedDecrease -= stopSpeedLines;
        Diamond.OnDiamondCaught -= onDiamondCaught;
        StoreController.OnPurchase -= updateDiamondCounter;
        SettingsController.OnDisplayGift -= GiftDiamonds;
    }

    void Start() {
        loader.SetActive(true);
        StartCoroutine(delayToSimulateServerRequest(2, displayKetchAd));
    }

    void displayKetchAd() {
        ketchAd.SetActive(true);
    }

    public void CloseKetchAd() {
        ketchAd.SetActive(false);
        StartCoroutine(delayToSimulateServerRequest(2, realStart));
    }

    void realStart() {
        setCounters();
        isStarted = false;

        loader.SetActive(false);
        showStartPanel();
        AudioManager.PlayMusic();
    }

    IEnumerator delayToSimulateServerRequest(float seconds, Action callback) {
        yield return new WaitForSeconds(seconds);
        callback();
    }

    void Update() {
        if (isStarted && !isPaused && !isGameOver) {
            totalPoints = Mathf.FloorToInt(totalPoints + Time.deltaTime * 100);
        }
        pointsCounter.text = totalPoints.ToString();
    }

    void setCounters() {
        updateHighScores();
        updateDiamondCounter();
    }

    void updateDiamondCounter() {
        diamondCounter.text = playerData.diamondCount.ToString();
    }

    void updateHighScores() {
        highscore.text = playerData.highScore.ToString();
        gameOverHighScore.text = playerData.highScore.ToString();
    }

    void startGame() {
        AudioManager.PlayMusic();
        pointsCanvas.DOFade(1, 0.5f).OnStart(() => pointsPanel.SetActive(true));
        pauseButton.DOFade(1, 0.5f).OnStart(() => pauseButton.gameObject.SetActive(true));
        ball.GetComponent<Ball>().StartMovingBall(vcam);
        isPaused = false;
        isGameOver = false;
        if (!isStarted) {
            totalPoints = 0;
            isStarted = true;
        }
    }

    void gameOver() {
        AudioManager.StopMusic();
        stopSpeedLines();
        flyingTimeCounter.gameObject.SetActive(false);
        if (totalPoints > playerData.highScore) {
            playerData.highScore = totalPoints;
            updateHighScores();
        }
        DataManager.SavePlayerData(playerData);
        vcam.Follow = null;
        vcam.LookAt = null;
        isGameOver = true;
        pauseButton.gameObject.SetActive(false);
        StartCoroutine(waitToShowGameOverPanel());
    }

    IEnumerator waitToShowGameOverPanel() {
        yield return new WaitForSeconds(0.5f);
        showGameOverPanel();
    }

    public void LaunchGame() {
        hideStartPanel(showTutorial);
    }

    public void ResumeGame() {
        hidePausePanel(showTutorial);
    }

    public void GoToMainMenu() {
        backLevelToClearState();
        levelGenerator.ClearLevel();
        hidePausePanel(showStartPanel);
    }

    public void ContinueGame() {
        loader.SetActive(true);
        pointsCanvas.DOFade(0, 0);
        continueCountdown.StopCountdown();
        instantiateBall();
        ball.GetComponent<Ball>().ResetBallPosition(new Vector3(0, levelGenerator.GetClosestPlatformY(), levelGenerator.GetClosestPlatformZBounds().x));
        levelGenerator.SetFirstPlatformInCenter();
        gameOverPanel.SetActive(false);
        StartCoroutine(showContinueAdAfter(0.5f));
    }

    IEnumerator showContinueAdAfter(float seconds) {
        yield return new WaitForSeconds(seconds);
        AdsManager.ShowRewardedVideo(() => {
            StartCoroutine(delayToSimulateServerRequest(0.5f, () => {
                loader.SetActive(false);
                showTutorial();
            }));
        });
    }

    public void RestartGame() {
        loader.SetActive(true);
        backLevelToClearState();
        gameOverPanel.SetActive(false);
        pausePanel.SetActive(false);
        StartCoroutine(delayToSimulateServerRequest(2, () => {
            loader.SetActive(false);
            showTutorial();
        }));
    }

    public void PauseGame() {
        isPaused = true;
        pauseButton.DOFade(0, 0.1f).OnComplete(() => pauseButton.gameObject.SetActive(false));
        pointsCanvas.DOFade(0, 0.1f).OnComplete(() => pointsPanel.SetActive(false));
        ball.GetComponent<Ball>().StopMovingBall();
        showPausePanel();
    }

    void instantiateBall() {
        if (!isStarted) {
            ball = Instantiate(ballPrefab);
            levelGenerator.GenerateLevel();
        }
        vcam.Follow = ball.transform;
        vcam.LookAt = ball.transform;
    }

    void backLevelToClearState() {
        isGameOver = true;
        levelGenerator.ClearLevel();
        Destroy(ball);
        totalPoints = 0;
        pointsCanvas.DOFade(0, 0);
        isStarted = false;
        isPaused = false;
    }

    void increaseSpeedLines() {
        var emission = speedLinesParticles.emission;
        if (!speedLinesParticles.isPlaying) {
            emission.rateOverTime = 200;
            speedLinesParticles.Play();
        } else {
            if(emission.rateOverTime.constant == 200) {
                emission.rateOverTime = 500;
            }else if(emission.rateOverTime.constant == 500) {
                emission.rateOverTime = 1000;
            }
        }
    }

    void stopSpeedLines() {
        speedLinesParticles.Stop();
    }

    void addPointsPerfectLanding(int points) {
        addPoints(points);
        perfectLandingCounter.text = "PERFECT LANDING +" + points;
        perfectLandingCounter.gameObject.SetActive(true);
        perfectLandingCounter.DOFade(0, 1).OnComplete(() => {
            perfectLandingCounter.gameObject.SetActive(false);
            perfectLandingCounter.DOFade(1, 0);
        });
    }

    void addPointsLanding(bool rightSide, int landingPoints, int flyingPoints) {
        flyingTimeCounter.gameObject.SetActive(false);
        addPoints(landingPoints);
        addPoints(flyingPoints);
        if (rightSide) {
            landingRightCounter.text = "+" + landingPoints;
            landingRightCounter.gameObject.SetActive(true);
            landingRightCounter.DOFade(0, 1).OnComplete(() => {
                landingRightCounter.gameObject.SetActive(false);
                landingRightCounter.DOFade(1, 0);
            });
        } else {
            landingLeftCounter.text = "+" + landingPoints;
            landingLeftCounter.gameObject.SetActive(true);
            landingLeftCounter.DOFade(0, 1).OnComplete(() => {
                landingLeftCounter.gameObject.SetActive(false);
                landingLeftCounter.DOFade(1, 0);
            });
        }
    }

    void onFlying(int flyingTime) {
        if (!isGameOver) {
            flyingTimeCounter.text = "FLYING TIME +" + flyingTime;
            flyingTimeCounter.gameObject.SetActive(true);
        }
    }

    void addPoints(int points) {
        totalPoints += points;
    }

    void onDiamondCaught(int points) {
        addPoints(points);
        playerData.diamondCount++;
        updateDiamondCounter();
    }

    public void ShrinkButtonScale(Transform buttonTransform) {
        buttonTransform.DOScale(Vector3.one * 0.9f, 0.1f);
    }

    public void UnshrinkButtonScale(Transform buttonTransform) {
        buttonTransform.DOScale(Vector3.one, 0.1f);
    }

    public void OpenUrl(string url) {
        Application.OpenURL(url);
    }

    public void WatchAdsToGetDiamonds(int diamondsAmount) {
        StartCoroutine(getNewDiamonds(diamondsAmount));
    }

    IEnumerator getNewDiamonds(int diamondsAmount) {
        loader.SetActive(true);
        yield return new WaitForSeconds(1);
        AdsManager.ShowRewardedVideo(() => {
            loader.SetActive(false);
            showNewDiamonds(diamondsAmount);
            playerData.diamondCount += diamondsAmount;
            updateDiamondCounter();
            DataManager.SavePlayerData(playerData);
        });
    }

    /*******************************************************************************UI**************************************************************************************/

    #region UI

    void hideUI() {
        ketchAd.SetActive(false);
        pausePanel.SetActive(false);
        tutorialPanel.SetActive(false);
        startPanel.SetActive(false);
        settingsPanel.SetActive(false);
        storePanel.SetActive(false);
        gameOverPanel.SetActive(false);
        newDiamondsPanel.SetActive(false);

        diamondCanvas.DOFade(0, 0);
        pointsPanel.SetActive(false);
        flyingTimeCounter.gameObject.SetActive(false);
        perfectLandingCounter.gameObject.SetActive(false);
        landingLeftCounter.gameObject.SetActive(false);
        landingRightCounter.gameObject.SetActive(false);

        pauseButton.gameObject.SetActive(false);
    }

    void showStartPanel() {
        background.SetActive(true);
        logo.DOFade(0, 0);
        logoRect.DOAnchorPosY(-350, 0);
        highscorePanel.DOAnchorPosY(150, 0);
        highscoreCanvas.DOFade(0, 0);
        buttonsPanelTransform.DOAnchorPosY(-500, 0);
        buttonsPanelCanvas.DOFade(0, 0);
        blueButtonsCanvas.DOFade(0, 0);
        pointsCanvas.DOFade(0, 0);
        foreach (var buttonTransform in blueButtonsTransform) {
            buttonTransform.DOAnchorPosY(-170, 0);
            buttonTransform.GetComponent<CanvasGroup>().DOFade(0, 0);
        }

        var sequence = DOTween.Sequence()
            //diamond
            .Insert(0, diamondCanvas.DOFade(1, 0.2f))
            //logo
            .Insert(0, logoRect.DOAnchorPosY(-500, 0.3f))
            .Insert(0.1f, logo.DOFade(1, 0.2f))
            //highscore
            .Insert(0, highscorePanel.DOAnchorPosY(0, 0.3f))
            .Insert(0.1f, highscoreCanvas.DOFade(1, 0.2f))
            //Buttons
            .Insert(0, buttonsPanelTransform.DOAnchorPosY(-350, 0.3f))
            .Insert(0.1f, buttonsPanelCanvas.DOFade(1, 0.2f))
            .Insert(0.1f, blueButtonsCanvas.DOFade(1, 0.2f))
            //callback
            .OnStart(() => {
                startPanel.SetActive(true);
                logo.gameObject.SetActive(true);
                highscorePanel.gameObject.SetActive(true);
                buttonsPanelTransform.gameObject.SetActive(true);
            })
            ;
        for (int i = 0; i < blueButtonsTransform.Length; i++) {
            sequence
                .Insert(0.2f + i * 0.1f, blueButtonsTransform[i].DOAnchorPosY(-85, 0.3f))
                .Insert(0.3f + i * 0.1f, blueButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(1, 0.2f));
        }

        sequence.Play();
    }

    void hideStartPanel(Action callback = null) {
        var sequence = DOTween.Sequence();

        for (int i = 0; i < blueButtonsTransform.Length; i++) {
            sequence
                .Insert(0 + i * 0.1f, blueButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(0, 0.2f))
                .Insert(0.1f + i * 0.1f, blueButtonsTransform[i].DOAnchorPosY(-170, 0.3f));
        }
        sequence
            //logo
            .Insert(0.2f, logoRect.DOAnchorPosY(-350, 0.3f))
            .Insert(0.3f, logo.DOFade(0, 0.2f))
            //highscore
            .Insert(0.2f, highscorePanel.DOAnchorPosY(150, 0.3f))
            .Insert(0.3f, highscoreCanvas.DOFade(0, 0.2f))
            //Buttons
            .Insert(0.2f, buttonsPanelTransform.DOAnchorPosY(-500, 0.3f))
            .Insert(0.3f, buttonsPanelCanvas.DOFade(0, 0.2f))
            .Insert(0.3f, blueButtonsCanvas.DOFade(0, 0.2f))
            //callback
            .OnComplete(() => {
                logo.gameObject.SetActive(false);
                highscorePanel.gameObject.SetActive(false);
                buttonsPanelTransform.gameObject.SetActive(false);
                startPanel.SetActive(false);
                callback?.Invoke();
            })
            ;

        sequence.Play();
    }

    void showTutorial() {
        if (!isStarted)
            instantiateBall();
        tutorialCanvas.DOFade(0, 0);
        gyroTutorial.SetActive(playerData.isUsingGyro);
        fingerTutorial.SetActive(!playerData.isUsingGyro);
        var sequence = DOTween.Sequence()
            .Append(tutorialCanvas.DOFade(1, 0.5f))
            .OnStart(() => tutorialPanel.SetActive(true))
            ;
    }

    public void HideTutorial() {
        var sequence = DOTween.Sequence()
            .Append(tutorialCanvas.DOFade(0, 0.1f))
            .OnComplete(() => {
                tutorialPanel.SetActive(false);
                background.SetActive(false);
                startGame();
            })
            ;
    }

    void showGameOverPanel(bool displayContinue = true) {
        background.SetActive(true);
        pointsPanel.SetActive(true);

        giftButton.DOFade(0, 0);
        giftButton.gameObject.SetActive(false);

        gameOverContinueContainer.gameObject.SetActive(displayContinue);
        if (displayContinue) {
            gameOverContinueContainer.DOAnchorPosY(-200, 0);
            gameOverContinueContainer.GetComponent<CanvasGroup>().DOFade(0, 0);
        }
        gameOverHighScoreTransform.DOAnchorPosY(400, 0);
        gameOverHighScoreTransform.GetComponent<CanvasGroup>().DOFade(0, 0);
        gameOverButtonsPanelTransform.DOAnchorPosY(-160, 0);
        gameOverButtonsPanelTransform.GetComponent<CanvasGroup>().DOFade(0, 0);
        gameOverBlueButtonsCanvas.DOFade(0, 0);
        foreach (var buttonTransform in gameOverblueButtonsTransform) {
            buttonTransform.DOAnchorPosY(-170, 0);
            buttonTransform.GetComponent<CanvasGroup>().DOFade(0, 0);
        }
        //gameOverChallengeContainer.DOAnchorPosY(-300, 0);

        var sequence = DOTween.Sequence();

        if (displayContinue) {
            sequence
            .Insert(0, gameOverContinueContainer.DOAnchorPosY(0, 0.3f))
            .Insert(0.1f, gameOverContinueContainer.GetComponent<CanvasGroup>().DOFade(1, 0.2f));
        }

        sequence
            .Insert(0, gameOverHighScoreTransform.DOAnchorPosY(560, 0.3f))
            .Insert(0.1f, gameOverHighScoreTransform.GetComponent<CanvasGroup>().DOFade(1, 0.2f))

            .OnStart(() => {
                gameOverPanel.SetActive(true);
                gameOverButtonsPanelTransform.gameObject.SetActive(false);
                //gameOverChallengeContainer.gameObject.SetActive(false);
                if (!displayContinue) {
                    ShowGameOverButtons(false);
                }
            })
            .OnComplete(() => {
                continueCountdown.StartCountdown(() => ShowGameOverButtons(true));
            })
            ;

        sequence.Play();
    }

    public void ShowGameOverButtons(bool isAfterContinue) {
        AudioManager.PlayMusic();
        continueCountdown.StopCountdown();
        var sequence = DOTween.Sequence();
        float startTime = 0;
        if (isAfterContinue) {
            startTime = 0.3f;
            sequence
                .Insert(0, gameOverContinueContainer.DOAnchorPosY(-200, 0.3f))
                .Insert(0.1f, gameOverContinueContainer.GetComponent<CanvasGroup>().DOFade(0, 0.2f));
        }
        sequence
            .Insert(startTime, gameOverButtonsPanelTransform.DOAnchorPosY(-10, 0.3f))
            .Insert(startTime + 0.1f, gameOverButtonsPanelTransform.GetComponent<CanvasGroup>().DOFade(1, 0.2f))
            .Insert(startTime + 0.1f, gameOverBlueButtonsCanvas.DOFade(1, 0.2f))

            .Insert(startTime + 0.1f, giftButton.DOFade(1, 0.2f))
            //.Insert(startTime + 0.1f, gameOverChallengeContainer.DOAnchorPosY(300, 0.2f))
            ;

        for (int i = 0; i < gameOverblueButtonsTransform.Length; i++) {
            sequence
                .Insert(startTime + 0.2f + i * 0.1f, gameOverblueButtonsTransform[i].DOAnchorPosY(-85, 0.3f))
                .Insert(startTime + 0.3f + i * 0.1f, gameOverblueButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(1, 0.2f))

            .OnStart(() => {
                giftButton.gameObject.SetActive(true);
                gameOverButtonsPanelTransform.gameObject.SetActive(true);
                //gameOverChallengeContainer.gameObject.SetActive(true);
            })
            .OnComplete(() => {
                gameOverContinueContainer.gameObject.SetActive(false);
            })
            ;

            sequence.Play();
        }
    }

    void hideGameOverPanel(Action callback = null) {
        var sequence = DOTween.Sequence();

        for (int i = 0; i < gameOverblueButtonsTransform.Length; i++) {
            sequence
                .Insert(0.2f + i * 0.1f, gameOverblueButtonsTransform[i].DOAnchorPosY(-170, 0.3f))
                .Insert(0.3f + i * 0.1f, gameOverblueButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(0, 0.2f));
        }
        sequence
            .Insert(0, giftButton.DOFade(0, 0.2f))
            //.Insert(0, gameOverChallengeContainer.DOAnchorPosY(-300, 0.2f))

            .Insert(0.1f, gameOverHighScoreTransform.DOAnchorPosY(400, 0.3f))
            .Insert(0.2f, gameOverHighScoreTransform.GetComponent<CanvasGroup>().DOFade(0, 0.2f))

            .Insert(0.1f, gameOverButtonsPanelTransform.DOAnchorPosY(-160, 0.3f))
            .Insert(0.2f, gameOverButtonsPanelTransform.GetComponent<CanvasGroup>().DOFade(0, 0.2f))
            .Insert(0.2f, gameOverBlueButtonsCanvas.DOFade(0, 0.2f))

            .OnComplete(() => {
                gameOverPanel.SetActive(false);
                callback?.Invoke();
            })
            ;

        sequence.Play();
    }

    void showPausePanel() {
        background.SetActive(true);
        pauseText.DOFade(0, 0);
        pauseText.GetComponent<RectTransform>().DOAnchorPosY(-400, 0);
        settingsButtonTransform.DOAnchorPosY(0, 0);
        settingsButtonTransform.GetComponent<Image>().DOFade(0, 0);
        foreach (var buttonTransform in pauseButtonsTransform) {
            buttonTransform.DOAnchorPosY(-150, 0);
            buttonTransform.GetComponent<CanvasGroup>().DOFade(0, 0);
        }
        pausePanel.SetActive(true);

        var sequence = DOTween.Sequence()
            //text
            .Insert(0, pauseText.GetComponent<RectTransform>().DOAnchorPosY(-530, 0.3f))
            .Insert(0.1f, pauseText.DOFade(1, 0.2f))
            //settings button
            .Insert(0, settingsButtonTransform.DOAnchorPosY(-130, 0.3f))
            .Insert(0.1f, settingsButtonTransform.GetComponent<Image>().DOFade(1, 0.2f))
            //callback
            .OnStart(() => {
                pauseText.gameObject.SetActive(true);
                settingsButtonTransform.gameObject.SetActive(true);
                pauseButtonsContainer.gameObject.SetActive(true);
            })
            ;
        for (int i = 0; i < pauseButtonsTransform.Length; i++) {
            sequence
                .Insert(0.2f + i * 0.1f, pauseButtonsTransform[i].DOAnchorPosY(0, 0.3f))
                .Insert(0.3f + i * 0.1f, pauseButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(1, 0.2f));
        }

        sequence.Play();
    }

    void hidePausePanel(Action callback = null) {
        var sequence = DOTween.Sequence();

        for (int i = 0; i < pauseButtonsTransform.Length; i++) {
            sequence
                .Insert(0f + (pauseButtonsTransform.Length - 1 - i) * 0.1f, pauseButtonsTransform[i].DOAnchorPosY(-150, 0.3f))
                .Insert(0.1f + (pauseButtonsTransform.Length - 1 - i) * 0.1f, pauseButtonsTransform[i].GetComponent<CanvasGroup>().DOFade(0, 0.2f));
        }

        sequence = DOTween.Sequence()
           //text
           .Insert(0.2f, pauseText.GetComponent<RectTransform>().DOAnchorPosY(-400, 0.3f))
           .Insert(0.3f, pauseText.DOFade(0, 0.2f))
           //settings button
           .Insert(0.2f, settingsButtonTransform.DOAnchorPosY(0, 0.3f))
           .Insert(0.3f, settingsButtonTransform.GetComponent<Image>().DOFade(0, 0.2f))
           //callback
           .OnComplete(() => {
               pauseText.gameObject.SetActive(false);
               settingsButtonTransform.gameObject.SetActive(false);
               pauseButtonsContainer.gameObject.SetActive(false);
               pausePanel.SetActive(false);
               callback?.Invoke();
           })
           ;

        sequence.Play();
    }

    public void OnStoreClicked() {
        if (isGameOver) {
            pointsPanel.SetActive(false);
            hideGameOverPanel(showStorePanel);
        } else {
            hideStartPanel(showStorePanel);
        }
    }

    void showStorePanel() {
        background.SetActive(true);
        storeTopTransform.DOAnchorPosY(300, 0);
        storeBottomTransform.DOAnchorPosY(-300, 0);
        watchAdsButton.DOAnchorPosY(100, 0);
        storeMiddle.SetActive(false);

        var sequence = DOTween.Sequence()
            .Insert(0, storeTopTransform.DOAnchorPosY(0, 0.2f))
            .Insert(0, storeBottomTransform.DOAnchorPosY(0, 0.2f))
            .Insert(1, watchAdsButton.DOAnchorPosY(-390, 0.3f))
            .OnStart(() => {
                storePanel.SetActive(true);
                storeMiddle.SetActive(true);
                storeController.OnBallsTabClicked();
            })
            ;
    }

    public void HideStorePanel() {
        storePanel.SetActive(false);
        if (isGameOver) {
            showGameOverPanel(false);
        } else {
            showStartPanel();
        }
    }

    public void OnSettingsClicked() {
        if (isGameOver) {
            pointsPanel.SetActive(false);
            hideGameOverPanel(showSettings);
        } else if (isPaused) {
            hidePausePanel(showSettings);
        } else {
            hideStartPanel(showSettings);
        }
    }

    void showSettings() {
        background.SetActive(true);
        settingsTopTransform.DOAnchorPosY(300, 0);
        settingsContainer.SetActive(false);
        settingsGPlayButton.SetActive(false);
        settingsBottom.SetActive(false);
        settingsSocialContainer.SetActive(false);
        socialRewardsTransform.gameObject.SetActive(false);
        socialRewardsTransform.DOAnchorPosY(-60, 0);

        for (int i = 0; i < settingsSocialButtons.Length; i++) {
            settingsSocialButtons[i].DOAnchorPosY(-235, 0);
            settingsSocialButtons[i].GetComponent<CanvasGroup>().DOFade(0, 0);
        }

        var sequence = DOTween.Sequence()
            .Insert(0, settingsTopTransform.DOAnchorPosY(0, 0.2f))
            .OnStart(() => {
                settingsPanel.SetActive(true);
                settingsContainer.SetActive(true);
                settingsGPlayButton.SetActive(true);
                settingsBottom.SetActive(true);
                settingsAds.SetActive(true);
                settingsStudioLogo.SetActive(true);
                settingsSocialContainer.SetActive(true);
            })
            .OnComplete(() => {
                socialRewardsTransform.gameObject.SetActive(true);
                socialRewardsTransform.DOAnchorPosY(-118, 0.2f);
            })
            ;

        for (int i = 0; i < settingsSocialButtons.Length; i++) {
            sequence
                .Insert(0.2f + i * 0.1f, settingsSocialButtons[i].DOAnchorPosY(-85, 0.3f))
                .Insert(0.3f + i * 0.1f, settingsSocialButtons[i].GetComponent<CanvasGroup>().DOFade(1, 0.2f));
        }

        sequence.Play();
    }

    public void HideSettingsPanel() {
        settingsPanel.SetActive(false);
        if (isGameOver) {
            showGameOverPanel(false);
        } else if (isPaused) {
            showPausePanel();
        } else {
            showStartPanel();
        }
    }

    public void GiftDiamonds(int amount) {
        playerData.diamondCount += amount;
        playerData.lastGiftTime = DateTime.Now;
        DataManager.SavePlayerData(playerData);
        updateDiamondCounter();
        showNewDiamonds(amount);
    }

    void showNewDiamonds(int count) {
        newDiamondsPanel.SetActive(false);
        newDiamondsValue.text = "+ " + count;
        newDiamondsContainer.DOFade(0, 0);
        newDiamondsContainer.transform.DOScale(0.7f, 0);
        newDiamondsGift.DOFade(0, 0);
        newDiamondsGift.transform.DOScale(0.7f, 0);

        var sequence = DOTween.Sequence()
            .Insert(0, newDiamondsGift.transform.DOScale(1, 0.2f))
            .Insert(0, newDiamondsGift.DOFade(1, 0.2f))
            .Insert(0.2f, newDiamondsContainer.transform.DOScale(1, 0.2f))
            .Insert(0.2f, newDiamondsContainer.DOFade(1, 0.2f))
            .OnStart(() => {
                newDiamondsPanel.SetActive(true);
            })
            ;

        sequence.Play();
        StartCoroutine(closeNewDiamonds());
    }

    IEnumerator closeNewDiamonds() {
        yield return new WaitForSeconds(1.5f);
        var sequence = DOTween.Sequence()
            .Insert(0, newDiamondsContainer.transform.DOScale(0.7f, 0.2f))
            .Insert(0, newDiamondsContainer.DOFade(0, 0.2f))
            .Insert(0.2f, newDiamondsGift.transform.DOScale(0.7f, 0.2f))
            .Insert(0.2f, newDiamondsGift.DOFade(0, 0.2f))
            .OnComplete(() => {
                newDiamondsPanel.SetActive(false);
            })
            ;

        sequence.Play();
    }

    #endregion

    /*************************************************************************************************************************************************************************************/
}
