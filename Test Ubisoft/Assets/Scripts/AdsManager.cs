﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using System;

public static class AdsManager {

    static string gameId = "3206371";
    static string rewardedPlacementId = "rewardedVideo";

    public static void InitAds() {
        Advertisement.Initialize(gameId);
    }

    public static void ShowRewardedVideo(Action callback) {
        ShowOptions options = new ShowOptions {
            resultCallback = (result) => handleRewardedVideoResult(result, callback)
        };
        Advertisement.Show(rewardedPlacementId, options);
    }

    static void handleRewardedVideoResult(ShowResult result, Action callback) {
        if (result == ShowResult.Finished) {
            callback();
        }
    }
}