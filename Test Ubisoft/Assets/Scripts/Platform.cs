﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Platform : MonoBehaviour {
    [SerializeField] int size;
    [SerializeField] Renderer meshRenderer;
    [SerializeField] MeshRenderer whiteBand;

    public delegate void PlatformDestroyed();
    public static event PlatformDestroyed OnPlatformDestroyed;

    float threshold = 10;
    float speed = 3;
    Transform ballTransform;
    Ball ball;
    bool hasToMove = false, moveToTheRight;
    Vector2 moveBounds;

    public void OnEnable() {
        Ball.OnPerfectLanding += displayWhiteBand;
        Ball.OnStopPerfectLanding += hideWhiteBand;
    }

    public void OnDisable() {
        Ball.OnPerfectLanding -= displayWhiteBand;
        Ball.OnStopPerfectLanding -= hideWhiteBand;
    }

    public void Start() {
        ballTransform = GameObject.FindGameObjectWithTag("Player").transform;
        ball = ballTransform.GetComponent<Ball>();
        whiteBand.material.DOFade(0, 0);
    }

    public void Update() {
        if (!ball.IsDead() && transform.position.z + size + threshold <= ballTransform.transform.position.z) {
            OnPlatformDestroyed?.Invoke();
            Destroy(gameObject);
        }
        if (hasToMove) {
            if (moveToTheRight)
                transform.Translate(Vector2.right * speed * Time.deltaTime);
            else
                transform.Translate(-Vector2.right * speed * Time.deltaTime);

            if (transform.position.x >= moveBounds.y) {
                moveToTheRight = false;
            }

            if (transform.position.x <= moveBounds.x) {
                moveToTheRight = true;
            }
        }
    }

    public void SetMaterial(Material material) {
        meshRenderer.material = material;
    }

    public int GetSize() {
        return size;
    }

    public void Move(bool move) {
        hasToMove = move;
        moveToTheRight = Random.Range(0, 100) % 2 == 0;
        moveBounds = new Vector2(transform.position.x - 3, transform.position.x + 3);
    }

    void displayWhiteBand(int points) {
        whiteBand.material.DOFade(0.3f, 0.3f);
    }

    void hideWhiteBand() {
        whiteBand.material.DOFade(0, 0.2f);
    }
}
