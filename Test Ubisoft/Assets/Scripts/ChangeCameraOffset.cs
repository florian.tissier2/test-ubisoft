﻿using UnityEngine;
using System.Collections;
using Cinemachine;

public class ChangeCameraOffset : MonoBehaviour {

    [SerializeField] CinemachineVirtualCamera vcam;
    [SerializeField] Vector3 offsetGrounded;
    [SerializeField] Vector3 offsetFlying;
    [SerializeField] float speed;

    bool move = false;
    float startTime;
    Vector3 target;
    float journeyLength;

    public void OnEnable() {
        Ball.IsGrounded += moveToGrounded;
        Ball.IsFlying += moveToFlying;
    }

    public void OnDisable() {
        Ball.IsGrounded -= moveToGrounded;
        Ball.IsFlying -= moveToFlying;
    }

    public void Update() {
        if (move) {
            var temp = vcam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;
            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            vcam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = Vector3.Lerp(temp, target, fracJourney);
            if (fracJourney >= 1)
                move = false;
        }
    }

    void moveToGrounded() {
        Debug.Log("moveToGrounded");
        move = true;
        target = offsetGrounded;
        startTime = Time.time;
        journeyLength = Vector3.Distance(vcam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset, target);
    }

    void moveToFlying() {
        Debug.Log("moveToGrounded");
        move = true;
        target = offsetFlying;
        startTime = Time.time;
        journeyLength = Vector3.Distance(vcam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset, target);
    }
}
