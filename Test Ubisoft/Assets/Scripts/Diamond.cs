﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour {
    public delegate void DiamondCaught(int points);
    public static event DiamondCaught OnDiamondCaught;

    [SerializeField] int points = 50;

    public void OnTriggerEnter(Collider other) {
        AudioManager.PlaySfx(Res.diamondSfx);
        OnDiamondCaught?.Invoke(points);
        Destroy(gameObject);
    }
}
