﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ShopItem : MonoBehaviour {
    [SerializeField] Image background;
    [SerializeField] Color selectedColor;
    [SerializeField] Color defaultColor;
    [SerializeField] Image image;
    [SerializeField] Sprite videoSprite;
    [SerializeField] Image priceBackground;
    [SerializeField] Color socialColor;
    [SerializeField] Color gemColor;
    [SerializeField] Color flagColor;
    [SerializeField] Color videoColor;
    [SerializeField] Color challengeColor;
    [SerializeField] GameObject gemPriceContainer;
    [SerializeField] Text gemPrice;
    [SerializeField] Text wordPrice;
    
    public bool isSelected, isDiscovered;
    public int price;
    public ShopItemType type;
    public Sprite sprite;

    public void SetShopItem(ShopItemType type, int price, Sprite sprite, bool isDiscovered, bool isSelected, Action callback) {
        this.type = type;
        this.price = price;
        this.isSelected = isSelected;
        this.isDiscovered = isDiscovered;
        this.sprite = sprite;

        background.GetComponent<Button>().onClick.AddListener(() => callback());

        switch (type) {
            case ShopItemType.Random:
                priceBackground.color = gemColor;
                gemPriceContainer.SetActive(false);
                wordPrice.gameObject.SetActive(true);
                wordPrice.text = "Random";
                break;
            case ShopItemType.Gem:
                priceBackground.color = gemColor;
                gemPriceContainer.SetActive(true);
                wordPrice.gameObject.SetActive(false);
                gemPrice.text = price.ToString();
                break;
            case ShopItemType.Flag:
                priceBackground.color = flagColor;
                gemPriceContainer.SetActive(true);
                wordPrice.gameObject.SetActive(false);
                gemPrice.text = price.ToString();
                break;
            case ShopItemType.Video:
                priceBackground.color = videoColor;
                gemPriceContainer.SetActive(false);
                wordPrice.gameObject.SetActive(true);
                wordPrice.text = price.ToString();
                image.sprite = videoSprite;
                break;
            case ShopItemType.Challenge:
                priceBackground.color = challengeColor;
                gemPriceContainer.SetActive(false);
                wordPrice.gameObject.SetActive(true);
                wordPrice.text = price.ToString();
                break;
        }

        if (isDiscovered) {
            if(type != ShopItemType.Random) {
                priceBackground.gameObject.SetActive(false);
            }
            if (isSelected) {
                background.color = selectedColor;
            }
            if (sprite != null) {
                image.sprite = sprite;
            }
        } else {
            background.color = defaultColor;
        }
    }

    public void Select() {
        if (type != ShopItemType.Random) {
            priceBackground.gameObject.SetActive(false);
            image.sprite = sprite;
        }
        background.color = selectedColor;
        isDiscovered = true;
        isSelected = true;
    }

    public void Unselect() {
        background.color = defaultColor;
        isSelected = false;
    }
}
