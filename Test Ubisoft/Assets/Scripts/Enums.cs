﻿using UnityEngine;
using System.Collections;

public enum PlatformType {
    Basic,
    BasicFullDiamond,
    BasicOneDiamond,
    BasicSmall,

    EndingBoard,
    EndingBoardOneDiamond,

    MiddleBoard,
    MiddleBoardOneDiamond,

    SpeedBoost,
    SpeedBoostOneDiamond
}

public enum ShopItemType {
    Random,
    Gem,
    Flag,
    Video,
    Challenge
}
