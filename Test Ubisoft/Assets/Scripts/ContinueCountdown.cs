﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ContinueCountdown : MonoBehaviour {

    [SerializeField] Image image;
    [SerializeField] float countdownTime = 5;

    Action callback;
    bool started;
    float startTime;

    void Start() {
        image.fillAmount = 1;
        started = false;
    }
    
    void Update() {
        if (started) {
            var time = Time.time - startTime;
            var percentage = time / countdownTime;
            image.fillAmount = 1 - percentage;
            if(percentage >= 1) {
                callback();
            }
        }
    }

    public void StartCountdown(Action callback) {
        this.callback = callback;
        startTime = Time.time;
        started = true;
    }

    public void StopCountdown() {
        started = false;
    }
}
