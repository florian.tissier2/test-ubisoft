﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {
    [Header("Level Configuration")]
    [SerializeField] Transform levelContainer;
    [SerializeField] float verticalSpacingBetweenPlatforms;
    [SerializeField] Vector2 horizontalIntervalForPlatforms;
    [SerializeField] float timeBeforeMediumDifficulty;// in seconds
    [SerializeField] float timeBeforeHardDifficulty;// in seconds

    [Header("Platforms")]
    [SerializeField] GameObject wallsPrefab;
    [SerializeField] PlatformType[] basicPlatforms;
    [SerializeField] PlatformType[] endingBoardPlatforms;
    [SerializeField] PlatformType[] middleBoardPlatforms;
    [SerializeField] PlatformType[] speedBoostPlatforms;

    Dictionary<PlatformType, GameObject> loadedPrefabs = new Dictionary<PlatformType, GameObject>();
    float nextPlatformNumber = 0;
    float nextZPosition = 0;
    float previousXPosition;
    Queue<Platform> platformsQueue = new Queue<Platform>();
    float closestPlatformY = 0;
    Vector2 closestPlatformZBounds;
    Material platformMaterial;

    public void OnEnable() {
        Platform.OnPlatformDestroyed += onPlatformDestroyed;
    }

    public void OnDisable() {
        Platform.OnPlatformDestroyed -= onPlatformDestroyed;
    }

    public void GenerateLevel() {
        ClearLevel();
        nextPlatformNumber = 0;
        nextZPosition = 0;
        previousXPosition = 0; closestPlatformY = 0;
        var activeBlock = GameController.playerData.activeBlock;
        if (GameController.playerData.activeBlock == "Random") {
            activeBlock = getRandomMaterial();
        }
        platformMaterial = Resources.Load<Material>(Res.platformMaterialsFolder + activeBlock);
        for (int i = 0; i < 10; i++) {
            createNextPlatform();
        }
    }

    public void SetFirstPlatformInCenter() {
        var go = platformsQueue.Peek().transform;
        go.GetComponent<Platform>().Move(false);
        var temp = go.position;
        temp.x = 0;
        go.position = temp;
    }

    void onPlatformDestroyed() {
        platformsQueue.Dequeue();
        var platform = platformsQueue.Peek();
        closestPlatformY = platform.transform.position.y;
        closestPlatformZBounds = new Vector2(platform.transform.position.z, platform.transform.position.z + platform.GetSize());
        createNextPlatform();
    }

    void createNextPlatform() {
        float x;
        if (nextPlatformNumber == 0) {
            x = 0;
        } else {
            x = Random.Range(0, 10) % 2 == 0 ? previousXPosition + 4.5f : previousXPosition - 4.5f; ;
            x = Mathf.Clamp(x, horizontalIntervalForPlatforms.x, horizontalIntervalForPlatforms.y);
            if (x == previousXPosition) {
                if (x == horizontalIntervalForPlatforms.x)
                    x = horizontalIntervalForPlatforms.x + 4.5f;
                else if (x == horizontalIntervalForPlatforms.y)
                    x = horizontalIntervalForPlatforms.y - 4.5f;
            }
        }

        var randomKind = Random.Range(0, 4);
        GameObject prefab;
        switch (randomKind) {
            case 0:
                prefab = getBasicPlatform();
                break;
            case 1:
                prefab = getEndingBoardPlatform();
                break;
            case 2:
                prefab = getMiddleBoardPlatform();
                break;
            case 3:
                prefab = getSpeedBoostPlatform();
                break;
            default:
                prefab = getBasicPlatform();
                break;
        }
        var platform = Instantiate(prefab, new Vector3(x, -verticalSpacingBetweenPlatforms * nextPlatformNumber, nextZPosition), Quaternion.identity, levelContainer);
        platform.GetComponent<Platform>().SetMaterial(platformMaterial);
        if (nextPlatformNumber > 20) {
            var move = Random.Range(0, 100) % 2 == 0;
            platform.GetComponent<Platform>().Move(move);
        }
        Instantiate(wallsPrefab, new Vector3(0, -verticalSpacingBetweenPlatforms * nextPlatformNumber, nextZPosition), Quaternion.identity, levelContainer);

        if (nextPlatformNumber == 0) {
            closestPlatformZBounds = new Vector2(0, platform.GetComponent<Platform>().GetSize());
        }
        nextZPosition += platform.GetComponent<Platform>().GetSize() + 15;
        nextPlatformNumber++;
        previousXPosition = x;
        platformsQueue.Enqueue(platform.GetComponent<Platform>());
    }

    GameObject getBasicPlatform() {
        var index = Random.Range(0, basicPlatforms.Length - 1);
        var platformType = basicPlatforms[index];
        GameObject prefab;
        if (loadedPrefabs.ContainsKey(platformType)) {
            prefab = loadedPrefabs[platformType];
        } else {
            prefab = Resources.Load<GameObject>(Res.basicPlatformsFolder + platformType.ToString());
            loadedPrefabs.Add(platformType, prefab);
        }
        return prefab;
    }

    GameObject getEndingBoardPlatform() {
        var index = Random.Range(0, endingBoardPlatforms.Length);
        var platformType = endingBoardPlatforms[index];
        GameObject prefab;
        if (loadedPrefabs.ContainsKey(platformType)) {
            prefab = loadedPrefabs[platformType];
        } else {
            prefab = Resources.Load<GameObject>(Res.endingBoardPlatformsFolder + platformType.ToString());
            loadedPrefabs.Add(platformType, prefab);
        }
        return prefab;
    }

    GameObject getMiddleBoardPlatform() {
        var index = Random.Range(0, middleBoardPlatforms.Length);
        var platformType = middleBoardPlatforms[index];
        GameObject prefab;
        if (loadedPrefabs.ContainsKey(platformType)) {
            prefab = loadedPrefabs[platformType];
        } else {
            prefab = Resources.Load<GameObject>(Res.middleBoardPlatformsFolder + platformType.ToString());
            loadedPrefabs.Add(platformType, prefab);
        }
        return prefab;
    }

    GameObject getSpeedBoostPlatform() {
        var index = Random.Range(0, speedBoostPlatforms.Length);
        var platformType = speedBoostPlatforms[index];
        GameObject prefab;
        if (loadedPrefabs.ContainsKey(platformType)) {
            prefab = loadedPrefabs[platformType];
        } else {
            prefab = Resources.Load<GameObject>(Res.speedBoostPlatformsFolder + platformType.ToString());
            loadedPrefabs.Add(platformType, prefab);
        }
        return prefab;
    }

    public float GetClosestPlatformY() {
        return closestPlatformY;
    }

    public Vector2 GetClosestPlatformZBounds() {
        return closestPlatformZBounds;
    }

    public void ClearLevel() {
        levelContainer.DeleteChildren();
        platformsQueue.Clear();
    }

    string getRandomMaterial() {
        var listAvailable = new List<string>();

        foreach (var shopObject in GameController.playerData.blocks) {
            if (shopObject.isDiscovered) {
                listAvailable.Add(shopObject.nameSprite);
            }
        }

        return listAvailable[Random.Range(0, listAvailable.Count - 1)];
    }
}
