﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class SwitchAnimator : MonoBehaviour {
    [SerializeField] Image background;
    [SerializeField] Text onText;
    [SerializeField] Color onColor;
    [SerializeField] Text offText;
    [SerializeField] Color offColor;
    [SerializeField] RectTransform square;

    bool isOn;

    public void Init(bool isOn) {
        if (isOn) {
            initOn();
        } else {
            initOff();
        }
    }

    void initOn() {
        background.color = onColor;
        onText.DOFade(1, 0);
        offText.DOFade(0, 0);
        square.DOAnchorPosX(51.25f, 0);
        isOn = true;
    }

    void initOff() {
        background.color = offColor;
        onText.DOFade(0, 0);
        offText.DOFade(1, 0);
        square.DOAnchorPosX(-51.25f, 0);
        isOn = false;
    }

    public void SetOn() {
        var sequence = DOTween.Sequence()
            .Insert(0, offText.DOFade(0, 0.3f))
            .Insert(0.1f, square.DOAnchorPosX(51.25f, 0.3f))
            .Insert(0.2f, onText.DOFade(1, 0.2f))
            .Insert(0.2f, background.DOColor(onColor, 0.2f))
            .OnComplete(() => isOn = true)
            ;
        sequence.Play();
    }

    public void SetOff() {
        var sequence = DOTween.Sequence()
            .Insert(0, onText.DOFade(0, 0.3f))
            .Insert(0.1f, square.DOAnchorPosX(-51.25f, 0.3f))
            .Insert(0.2f, offText.DOFade(1, 0.2f))
            .Insert(0.2f, background.DOColor(offColor, 0.2f))
            .OnComplete(() => isOn = false)
            ;
        sequence.Play();
    }

    public void OnSwitchClick() {
        if (isOn) {
            SetOff();
        } else {
            SetOn();
        }
    }
}
