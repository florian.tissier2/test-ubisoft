﻿using UnityEngine;
using System.Collections;

public class SettingsController : MonoBehaviour {

    [SerializeField] SwitchAnimator settingsSwitch;
    [SerializeField] SwitchAnimator musicSwitch;
    [SerializeField] SwitchAnimator gyroSwitch;
    [SerializeField] GameObject gyroSetting;
    [SerializeField] GameObject facebookReward;
    [SerializeField] GameObject twitterReward;
    [SerializeField] GameObject instagramReward;

    public delegate void DisplayGift(int reward);
    public static event DisplayGift OnDisplayGift;

    bool waitingForGift;

    public void Start() {
        settingsSwitch.Init(GameController.playerData.canPlaySfx);
        musicSwitch.Init(GameController.playerData.canPlayMusic);
        gyroSwitch.Init(GameController.playerData.isUsingGyro);
        gyroSetting.SetActive(SystemInfo.supportsGyroscope);
        facebookReward.SetActive(!GameController.playerData.gotFacebookGift);
        twitterReward.SetActive(!GameController.playerData.gotTwitterGift);
        instagramReward.SetActive(!GameController.playerData.gotInstagramGift);
    }

    public void OnSettingsSwitchClicked() {
        GameController.playerData.canPlaySfx = !GameController.playerData.canPlaySfx;
        DataManager.SavePlayerData(GameController.playerData);
    }

    public void OnMusicSettingsClicked() {
        GameController.playerData.canPlayMusic = !GameController.playerData.canPlayMusic;
        DataManager.SavePlayerData(GameController.playerData);
        AudioManager.MusicStateChanged();
    }

    public void OnGyroSettingsClicked() {
        GameController.playerData.isUsingGyro = !GameController.playerData.isUsingGyro;
        DataManager.SavePlayerData(GameController.playerData);
    }

    public void OnApplicationFocus(bool focus) {
        if(focus && waitingForGift) {
            waitingForGift = false;
            OnDisplayGift?.Invoke(50);
        }
    }

    public void OpenFacebookWithGift() {
        if (!GameController.playerData.gotFacebookGift) {
            waitingForGift = true;
            GameController.playerData.gotFacebookGift = true;
            facebookReward.SetActive(false);
        }
        Application.OpenURL("https://facebook.com");
    }

    public void OpenTwitterWithGift() {
        if (!GameController.playerData.gotTwitterGift) {
            waitingForGift = true;
            GameController.playerData.gotTwitterGift = true;
            twitterReward.SetActive(false);
        }
        Application.OpenURL("https://twitter.com");
    }

    public void OpenInstagramWithGift() {
        if (!GameController.playerData.gotInstagramGift) {
            waitingForGift = true;
            GameController.playerData.gotInstagramGift = true;
            instagramReward.SetActive(false);
        }
        Application.OpenURL("https://instagram.com");
    }
}
