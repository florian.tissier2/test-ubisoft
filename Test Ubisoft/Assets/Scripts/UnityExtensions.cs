﻿using UnityEngine;
using System.Collections;

public static class UnityExtensions {

    public static void DeleteChildren(this Transform transform) {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
    }


    //found at https://stackoverflow.com/questions/3451553/value-remapping
    public static float remap(this float value, float fromMin, float fromMax, float toMin, float toMax) {
        return toMin + (toMax - toMin) * ((value - fromMin) / (fromMax - fromMin));
    }
}
