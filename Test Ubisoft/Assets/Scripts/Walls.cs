﻿using UnityEngine;
using System.Collections;

public class Walls : MonoBehaviour {

    Transform ballTransform;
    Ball ball;

    void Start() {
        ballTransform = GameObject.FindGameObjectWithTag("Player").transform;
        ball = ballTransform.GetComponent<Ball>();
    }

    public void Update() {
        if (!ball.IsDead() && transform.position.z + 70 <= ballTransform.transform.position.z) {
            Destroy(gameObject);
        }
    }
}
