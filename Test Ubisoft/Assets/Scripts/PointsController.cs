﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsController : MonoBehaviour
{
    [SerializeField] Text points;

    public void Start() {
        gameObject.SetActive(false);
    }

    public void UpdatePoints( int points) {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        if (points == 0)
            this.points.text = "";
        this.points.text = points.ToString();
    }
}
